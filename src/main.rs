extern crate graph;
extern crate time;
extern crate core;

use graph::Graph;
use graph::Edge;
use graph::graphs::UndirectedAdjacencyListGraph;
use graph::graphs::UnweightedEdge;

use std::old_io::BufferedReader;
use std::old_io::File;
use std::hash::Hash;
use std::collections::hash_map::Hasher;
use std::time::duration::Duration;

use time::Timespec;

use core::cmp::Eq;
use core::ops::Sub;

fn main() {

    let filename = String::from_str("201411.link.v4");
    println!("Graph data source: {}", filename);

    let graph = make_graph_from_file(filename);
    println!("{} vertices in graph.", graph.get_nodes().len());
    println!("{} edges in graph.", graph.get_edges().len());

    // benchmark_dijkstras_shortest_path(&graph, 5us);
    // benchmark_dijkstras_shortest_paths(&graph, 5us);
    // benchmark_diameter_path(&graph, 5us);
    benchmark_k_core_decomposition(&graph, 5us);
    benchmark_kruskal_min_spanning_tree(&graph, 5us);
}

fn make_graph_from_file(filename: String) -> UndirectedAdjacencyListGraph<u32, UnweightedEdge<u32>> {
    let mut graph : UndirectedAdjacencyListGraph<u32, UnweightedEdge<u32>> = Graph::new();

    let path = Path::new(filename);
    let mut file = BufferedReader::new(File::open(&path));

    for line in file.lines() {
        let line_copy = line.unwrap().clone();

        let v: Vec<&str> = line_copy.split('\t').collect();

        let first_id = v[0].parse::<u32>().unwrap();
        
        let second_id = v[1].parse::<u32>().unwrap();

        if !graph.is_node_in_graph(&first_id) {
            graph.add_node(first_id.clone());
        }

        if !graph.is_node_in_graph(&second_id) {
            graph.add_node(second_id.clone());
        }

        graph.add_edge(first_id.clone(), second_id.clone(), 0);
    }

    graph
}

fn benchmark_dijkstras_shortest_path<N, E, G>(graph: &G, runs: usize) -> Vec<Duration>
    where N: Eq + Clone + Hash<Hasher>,
          E: Eq + Clone + Hash<Hasher> + Edge<N>,
          G: Graph<N, E>
{
    println!("Benchmarking dijkstras_shortest_path function");
    let mut result = Vec::with_capacity(runs);

    let ids = graph.get_nodes();
    let id_one = &ids[0];
    let id_two = &ids[1];

    for i in range(0, runs) {
        println!("Starting iteration {}", i);

        let start_time = time::get_time();

        graph.dijkstras_shortest_path(id_one, id_two);

        let end_time = time::get_time();

        let duration = end_time.sub(start_time);

        result.push(duration);
    }

    println!("benchmark_dijkstras_shortest_path result for {} iterations: ", runs);
    for run in result.iter() {
        println!("{} seconds | {} milliseconds", run.num_seconds(), run.num_milliseconds());
    }

    result
}

fn benchmark_dijkstras_shortest_paths<N, E, G>(graph: &G, runs: usize) -> Vec<Duration> 
    where N: Eq + Clone + Hash<Hasher>,
          E: Eq + Clone + Hash<Hasher> + Edge<N>,
          G: Graph<N, E>
{
    println!("Benchmarking dijkstras_shortest_paths function");

    let mut result = Vec::with_capacity(runs);

    let ids = graph.get_nodes();
    let id = &ids[0];

    for i in range(0, runs) {
        println!("Starting iteration {}", i);

        let start_time = time::get_time();

        graph.dijkstras_shortest_paths(id);

        let end_time = time::get_time();

        let duration = end_time.sub(start_time);

        result.push(duration);
    }

    println!("benchmark_dijkstras_shortest_paths result for {} iterations:", runs);
    for run in result.iter() {
        println!("{} seconds | {} milliseconds", run.num_seconds(), run.num_milliseconds());
    }
    
    result
}

fn benchmark_diameter_path<N, E, G>(graph: &G, runs: usize) -> Vec<Duration> 
    where N: Eq + Clone + Hash<Hasher>,
          E: Eq + Clone + Hash<Hasher> + Edge<N>,
          G: Graph<N, E>
{
    println!("Benchmarking diameter_path function");

    let mut result = Vec::with_capacity(runs);

    for i in range(0, runs) {
        println!("Starting iteration {}", i);

        let start_time = time::get_time();

        graph.diameter_path();

        let end_time = time::get_time();

        let duration = end_time.sub(start_time);
        
        result.push(duration);
    }

    println!("benchmark_diameter_path result for {} iterations:", runs);
    for run in result.iter() {
        println!("{} seconds | {} milliseconds", run.num_seconds(), run.num_milliseconds());
    }
    
    result
}

fn benchmark_k_core_decomposition<N, E, G>(graph: &G, runs: usize) -> Vec<Duration>
    where N: Eq + Clone + Hash<Hasher>,
          E: Eq + Clone + Hash<Hasher> + Edge<N>,
          G: Graph<N, E>
{
    println!("Benchmarking k_core_decomposition function");

    let mut result = Vec::with_capacity(runs);

    for i in range(0, runs) {
        println!("Starting iteration {}", i);

        let start_time = time::get_time();

        graph.k_core_decomposition();

        let end_time = time::get_time();

        let duration = end_time.sub(start_time);

        result.push(duration);
    }

    println!("benchmark_k_core_decomposition result for {} iterations:", runs);
    for run in result.iter() {
        println!("{} seconds | {} milliseconds", run.num_seconds(), run.num_milliseconds());
    }
    
    result
}

fn benchmark_kruskal_min_spanning_tree<N, E, G>(graph: &G, runs: usize) -> Vec<Duration>
    where N: Eq + Clone + Hash<Hasher>,
          E: Eq + Clone + Hash<Hasher> + Edge<N>,
          G: Graph<N, E>
{
    println!("Benchmarking kruskal_min_spanning_tree function");

    let mut result = Vec::with_capacity(runs);

    for i in range(0, runs) {
        println!("Starting iteration {}", i);

        let start_time = time::get_time();

        let mst = graph.kruskal_min_spanning_tree();

        let end_time = time::get_time();

        let duration = end_time.sub(start_time);

        result.push(duration);
    }

    println!("benchmark_kruskal_min_spanning_tree result for {} iterations:", runs);
    for run in result.iter() {
        println!("{} seconds | {} milliseconds", run.num_seconds(), run.num_milliseconds());
    }
    
    result
}